'use strict';

angular.module('testApp.factories', []).
factory('profileFactory', function($q, $http) {
    var buffer = {
        getProfile: function() {
            var defer = $q.defer();

            // $http({
                // method: 'GET',
                // url: 'http://applicant.pointsource.com/api/v1/902289ef-0d69-4bb1-8392-d65f1b668a5d'
            // }).
            // success(function(data, status, headers, config) {
                defer.resolve();
            // });

            return defer.promise;
        }
    }

    return buffer;
});
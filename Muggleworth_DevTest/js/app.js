/*
The following was a developers test from when I interned at PointSource, LLC.

The link for retrieving the profileData object is no longer active, so I have commented that out of the Angular factory.

As a result, running this page will throw the following error:
"Error: profileData is undefined"

This is still a valid example of being able to design a web page using HTML and CSS and provide a small demonstration of my knowledge of AngularJS, albeit in the code but no longer when running.
*/

'use strict';

angular.module('testApp', [
    'ngRoute',
    'testApp.factories'
]).
config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/main/', {
                templateUrl: 'pages/main.html',
                controller: 'TestCtrl',
                resolve: {
                    profileData: function(profileFactory) {
                        console.log("buffering");
                        return profileFactory.getProfile();
                    }
                }
            })
    }
]).
controller('TestCtrl', function($scope, profileData) {
    if (profileData.person.sex == "male") {
        $scope.gender = 'boy';
    } else {
        $scope.gender = 'girl';
    }
    $scope.name = profileData.person.name["first-name"] + " " + profileData.person.name["last-name"];
    if (profileData.person.address["apt-#"]) {
        $scope.addressLine1 = profileData.person.address["street-number"] + " " + profileData.person.address["street"] + " Unit " + profileData.person.address["apt-#"];
    } else {
        $scope.addressLine1 = profileData.person.address["street-number"] + " " + profileData.person.address["street"];
    }
    $scope.addressLine2 = profileData.person.address["city"] + ", " + profileData.person.address["state"];
    $scope.addressLine3 = profileData.person.address["zip-code"];

});
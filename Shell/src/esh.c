/**
 * Team Members: Sam Muggleworth (sammugg) & Nathaniel Hughes (njh2986)
 *
 */

/*
 * esh - the 'pluggable' shell.
 *
 * Developed by Godmar Back for CS 3214 Fall 2009
 * Virginia Tech.
 */
#include <stdio.h>
#include <readline/readline.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "esh.h"
#include "esh-sys-utils.h"

static void print_jobs(void);
static void print_commands(struct esh_pipeline *p);
static struct esh_pipeline *find_jid(int jid);
static void child_status_change(pid_t cpid, int status);
static struct list jobs;
static struct termios *term;
static pid_t termId;

static void
usage(char *progname)
{
    printf("Usage: %s -h\n"
           " -h            print this help\n"
           " -p  plugindir directory from which to load plug-ins\n",
           progname);

    exit(EXIT_SUCCESS);
}

/* Build a prompt by assembling fragments from loaded plugins that
 * implement 'make_prompt.'
 *
 * This function demonstrates how to iterate over all loaded plugins.
 */
static char *
build_prompt_from_plugins(void)
{
    char *prompt = NULL;
    struct list_elem *e = list_begin(&esh_plugin_list);

    for (; e != list_end(&esh_plugin_list); e = list_next(e))
    {
        struct esh_plugin *plugin = list_entry(e, struct esh_plugin, elem);

        if (plugin->make_prompt == NULL)
            continue;

        /* append prompt fragment created by plug-in */
        char *p = plugin->make_prompt();
        if (prompt == NULL)
        {
            prompt = p;
        }
        else
        {
            prompt = realloc(prompt, strlen(prompt) + strlen(p) + 1);
            strcat(prompt, p);
            free(p);
        }
    }

    /* default prompt */
    if (prompt == NULL)
        prompt = strdup("esh> ");

    return prompt;
}

/* The shell object plugins use.
 * Some methods are set to defaults.
 */
struct esh_shell shell =
{
    .build_prompt = build_prompt_from_plugins,
    .readline = readline,       /* GNU readline(3) */
    .parse_command_line = esh_parse_command_line /* Default parser */
};

/*
 * Functions you may use for your shell.
 * See FAQ Question 4.
 *
 * CS 3214 Fall 2011, Godmar Back.
 */

/**
 * Assign ownership of ther terminal to process group
 * pgrp, restoring its terminal state if provided.
 *
 * Before printing a new prompt, the shell should
 * invoke this function with its own process group
 * id (obtained on startup via getpgrp()) and a
 * sane terminal state (obtained on startup via
 * esh_sys_tty_init()).
 */
static void give_terminal_to(pid_t pgrp, struct termios *pg_tty_state)
{
    esh_signal_block(SIGTTOU);
    int rc = tcsetpgrp(esh_sys_tty_getfd(), pgrp);
    if (rc == -1)
        esh_sys_fatal_error("tcsetpgrp: ");

    if (pg_tty_state)
        esh_sys_tty_restore(pg_tty_state);
    esh_signal_unblock(SIGTTOU);
}

/*
 * Functions you may use for your shell.
 * See FAQ Question 24.
 *
 * CS 3214 Fall 2011, Godmar Back.
 */

/*
 * signal handler.
 * Call waitpid() to learn about any child processes that
 * have exited or changed status (been stopped, needed the
 * terminal, etc.)
 * Just record the information by updating the job list
 * data structures.  Since the call may be spurious (e.g.
 * an already pending SIGCHLD is delivered even though
 * a foreground process was already reaped), ignore when
 * waitpid returns -1.
 * Use a loop with WNOHANG since only a single SIGCHLD
 * signal may be delivered for multiple children that have
 * exited.
 */
static void sig_handler(int sig, siginfo_t *info, void *_ctxt)
{
    pid_t child;
    int status;

    assert((sig == SIGCHLD));

    if (sig == SIGCHLD)
    {
        while ((child = waitpid(-1, &status, WUNTRACED | WNOHANG)) > 0)
        {
            child_status_change(child, status);
        }
    }

    return;
}

/**
 * Processes and responds to a childs status change
 * @param cpid is the pid of the child changed
 * @param status is the integer status code indicating a change in child state
 */
static void child_status_change(pid_t cpid, int status) {

    //check for error
    if (cpid < 0) {
        esh_sys_fatal_error("Fatal error while processing status change.");
    }

    //Find the job->command with cpid
    struct list_elem *e;
    for (e = list_begin(&jobs); e != list_end(&jobs); e = list_next(e)) {
        struct esh_pipeline *p = list_entry(e, struct esh_pipeline, elem);
        struct list_elem *f;
        for (f = list_begin(&p->commands); f != list_end(&p->commands); f = list_next(f)) {
            struct esh_command *c = list_entry(f, struct esh_command, elem);

            if (c->pid == cpid) {
                if (WIFEXITED(status)) {
                    //process complete or ended
                    list_remove(f);
                }
                else if (WIFSTOPPED(status)) {
                    //process stopped/suspended by ^Z (SIGTSTP)
                    p->status = STOPPED;
                    esh_sys_tty_save(&p->saved_tty_state);
                    printf("[%d]+ Stopped              (", p->jid);
                    print_commands(p);
                    printf(")\n");
                    give_terminal_to(termId, term);
                } else {
                    switch WTERMSIG(status) {
                    case 23:
                        //SIGSTOP (stop command)
                        p->status = STOPPED;
                        esh_sys_tty_save(&p->saved_tty_state);
                        give_terminal_to(termId, term);
                        break;
                    case 9:
                        //SIGKILL (kill command)
                        //remove command from list
                        list_remove(f);
                        give_terminal_to(termId, term);
                        break;
                    case 2:
                        //SIGINT (^C)
                        //remove command from list
                        list_remove(f);
                        give_terminal_to(termId, term);
                        break;
                    }
                }

                //check if there are any more commands, if none remove job
                if (list_empty(&p->commands)) {
                    list_remove(e);
                }
            }
        }
    }
}

/* Wait for all processes in this pipeline to complete, or for
 * the pipeline's process group to no longer be the foreground
 * process group.
 * You should call this function from a) where you wait for
 * jobs started without the &; and b) where you implement the
 * 'fg' command.
 *
 * Implement child_status_change such that it records the
 * information obtained from waitpid() for pid 'child.'
 * If a child has exited or terminated (but not stopped!)
 * it should be removed from the list of commands of its
 * pipeline data structure so that an empty list is obtained
 * if all processes that are part of a pipeline have
 * terminated.  If you use a different approach to keep
 * track of commands, adjust the code accordingly.
 */
static void wait_for_job(struct esh_pipeline *pipeline)
{
    assert(esh_signal_is_blocked(SIGCHLD));

    while (pipeline->status == FOREGROUND && !list_empty(&pipeline->commands))
    {
        int status;

        pid_t child = waitpid(-1, &status, WUNTRACED);
        if (child != -1)
            child_status_change(child, status);
        printf("Child processing\n");
    }
}

/*
 * main takes commane line args from executing ./esh and runs the program as specified
 * The main function contains a for loop that runs indefinitely creating a prompt, reading
 * user input and running commands appropriately (including responding to job control
 * by the user)
 */
int
main(int ac, char *av[])
{
    int opt;
    list_init(&esh_plugin_list);

    //init list of jobs
    list_init(&jobs);

    term = esh_sys_tty_init(); //initialize our terminal
    setpgid(0, 0); //Init group for new terminal
    termId = getpid(); //store terminal PID
    give_terminal_to(termId, term); //get control of the terminal

    /* Process command-line arguments. See getopt(3) */
    while ((opt = getopt(ac, av, "hp:")) > 0)
    {
        switch (opt)
        {
        case 'h':
            usage(av[0]);
            break;

        case 'p':
            esh_plugin_load_from_directory(optarg);
            break;
        }
    }

    //initialize shell
    esh_plugin_initialize(&shell);

    //redirect signals to handler
    esh_signal_sethandler(SIGCHLD, sig_handler);

    //local vars for processing jobs/commands
    struct list_elem *p; //list element for job
    struct list_elem *placehold; //when job is removed from cline to add to 'jobs', next pipeline is saved here
    struct esh_pipeline *pipe; //job being processed
    struct list_elem *comm; //list element for command
    struct esh_command *command; //command being processed
    char *c;  //string name of command being processed :: command->argv[0]
    int jid; //jid of job being controlled
    char *arg; //string of second arg of command being processed :: command->argv[1]
    bool letKillStop = false; //in processing fg/bg/kill/stop, if there is no second arg don't let kill/stop run
    bool executedPlugin = false; //boolean for whether or not a plugin was run.
    bool jobAdded = false;

    /* Read/eval loop. */
    for (;;)
    {
        /* Do not output a prompt unless shell's stdin is a terminal */
        char *prompt = isatty(0) ? shell.build_prompt() : NULL;
        char *cmdline = shell.readline(prompt);
        free (prompt);

        if (cmdline == NULL)  /* User typed EOF */
            break;

        struct esh_command_line *cline = shell.parse_command_line(cmdline);
        free (cmdline);
        if (cline == NULL)                  /* Error in command line */
            continue;

        if (list_empty(&cline->pipes))      /* User hit enter */
        {
            esh_command_line_free(cline);
            continue;
        }

        //esh_command_line_print(cline); //this prints results as seen in sample

        //User has hit enter; need to iterate through cline and get/exec jobs
        //jobs are ; separated
        p = list_begin(&cline->pipes);

        while (p != list_end(&cline->pipes)) {
            placehold = list_next(p);
            pipe = list_entry(p, struct esh_pipeline, elem); //curr pipe
            pipe->jid = 0; //init jid to 0; next command won't try to reset jid if multiple commands in one pipeline
            comm = list_begin(&pipe->commands); //get first command (NOTE: WE DON'T SUPPORT PIPES: |)
            jobAdded = false;

            //block sigchld while getting ready to ex. commands (built in or not)
            esh_signal_block(SIGCHLD);

            //iterate through commands in pipeline, feeding output of previous as input of next
            //commands are | separated;
            for (; comm != list_end(&pipe->commands); comm = list_next(comm))
            {
                command = list_entry(comm, struct esh_command, elem); //curr command being processed
                c = command->argv[0];

                //Check for BuiltIn comamand, execute if match else check for plugins
                if (!strcmp(c, "exit"))
                {
                    if (list_empty(&jobs)) {
                        exit(EXIT_SUCCESS);
                    }
                    else {
                        printf("There are active jobs.\n");
                    }
                }
                else if (!strcmp(c, "jobs"))
                {
                    print_jobs();
                }
                else if (!strcmp(c, "fg") || !strcmp(c, "bg") || !strcmp(c, "kill") || !strcmp(c, "stop"))
                {
                    letKillStop = false;

                    if (!list_empty(&jobs))
                    {
                        struct esh_pipeline *job; //pipe to control

                        //there are 1+ active jobs
                        if ((arg = command->argv[1]) != NULL)
                        {
                            //try to find % which would head the jid
                            if (!strncmp(arg, "%%", 1))
                            {
                                char *token = strtok(arg, "%%");
                                jid = atoi(token);
                                letKillStop = true;
                            }
                            else
                            {
                                printf("%s: Need %% sign to process job ID\n", c);
                                continue;
                            }

                            //search for job with that jid
                            if ((job = find_jid(jid)) == NULL)
                            {
                                //job with jid == jid not found. Output result
                                printf("%s: %d: no such job\n", c, jid);
                                continue;
                            }
                        }
                        else
                        {
                            // no second arg, cannot kill/stop but can fg/bg most recent job
                            struct list_elem *e = list_back(&jobs);
                            job = list_entry(e, struct esh_pipeline, elem);
                        }

                        if (!strcmp(c, "fg"))
                        {
                            //fg stuff
                            //bring job to fg, print command, make shell wait for it's stopping/killing/completion
                            print_commands(job);
                            printf("\n");
                            esh_signal_block(SIGCHLD);

                            job->status = FOREGROUND;

                            give_terminal_to(job->pgrp, term);

                            if (kill(-job->pgrp, SIGCONT) < 0)
                            {
                                esh_sys_fatal_error("Could not SIGCONT");
                            }

                            wait_for_job(job);

                            give_terminal_to(termId, term);

                            esh_signal_unblock(SIGCHLD);
                        }
                        else if (!strcmp(c, "bg"))
                        {
                            job->status = BACKGROUND;

                            if (kill(-job->pgrp, SIGCONT) < 0)
                            {
                                esh_sys_fatal_error("Could not SIGCONT");
                            }
                        }
                        else if (!strcmp(c, "kill") && letKillStop == true)
                        {
                            //job stat = terminated
                            if (kill(-job->pgrp, SIGKILL) < 0)
                            {
                                esh_sys_fatal_error("Could not KILL");
                            }
                        }
                        else if (!strcmp(c, "stop") && letKillStop == true)
                        {
                            //job stat = stopped (paused)
                            if (kill(-job->pgrp, SIGSTOP) < 0)
                            {
                                esh_sys_fatal_error("Could not STOP");
                            }
                        }
                        else
                        {
                            printf("%s: No job specified\n", c);
                            continue;
                        }
                    }
                    else
                    {
                        if (!strcmp(c, "fg"))
                        {
                            printf("fg: current: no such job\n");
                        }
                        else if (!strcmp(c, "bg"))
                        {
                            printf("bg: current: no such job\n");
                        }
                        else if (!strcmp(c, "kill"))
                        {
                            printf("kill: usage: kill %%[job id]\n");
                        }
                        else if (!strcmp(c, "stop"))
                        {
                            printf("stop: missing job id\n");
                        }
                    }
                }
                else
                {
                    struct list_elem *e = list_begin(&esh_plugin_list);

                    executedPlugin = false;

                    for (; e != list_end(&esh_plugin_list); e = list_next(e))
                    {
                        struct esh_plugin *plugin = list_entry(e, struct esh_plugin, elem);
                        if (plugin->process_builtin == NULL)
                        {
                            continue;
                        }

                        if (plugin->process_builtin(command)) // parameter: esh_command*
                        {
                            executedPlugin = true;
                        }
                    }

                    if (!executedPlugin)
                    {
                        /*Not a built in plugin, so use execvp to run the command*/

                        //give job id
                        if (pipe->jid == 0) {
                            if (list_empty(&jobs)) {
                                pipe->jid = 1;
                            }
                            else {
                                pipe->jid = list_size(&jobs) + 1;
                            }
                        }
                        pipe->pgrp = -1;
                        // if pgrp of pipe is -1 need to set command as new pgrp;
                        // else this pipe is already grouped and all commands left in it should be put in the same group

                        pid_t child;

                        if ((child = fork()) < 0) {
                            //There's been a problem trying to fork
                            esh_sys_fatal_error("Couldn't fork to run command");
                        } else if (child == 0) {
                            //this is the child/job

                            command->pid = getpid(); //current command pid == child PID

                            if (pipe->pgrp == -1) {
                                pipe->pgrp = getpid();
                            }

                            //set pgid to be it's own pid
                            if (setpgid(0, 0) < 0) {
                                esh_sys_fatal_error("Could not set pgid");
                            }

                            //BACKGROUND V FOREGROUND
                            if (pipe->bg_job) {
                                pipe->status = BACKGROUND;
                            }
                            else {
                                //foreground job, give it the terminal
                                pipe->status = FOREGROUND;
                                give_terminal_to(pipe->pgrp, term);
                            }

                            // I/O Redirection here

                            // esh_command *command knows if there is stdin or stdout redirection
                            // and where to redirect to

                            bool redirectedOut = false;
                            bool redirectedIn = false;

                            int in; // input file fd if there's an input file
                            int out; // output file fd if there's an output file

                            if (command->iored_output)
                            {
                                //append_to_output in esh_command struct

                                char *fileTowrite = command->iored_output;

                                if (command->append_to_output) {
				  out = open(fileTowrite, O_WRONLY | O_APPEND | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);
				}
                                else {
				  out = open(fileTowrite, O_WRONLY | O_TRUNC | O_CREAT, S_IRUSR | S_IRGRP | S_IWGRP | S_IWUSR);
				}

                                if (out < 0)
                                {
                                    esh_sys_fatal_error("could not open file for writing");
                                }

                                // replace standard output with output file
                                dup2(out, 1);

                                // set redirected output flag for cleanup
                                redirectedOut = true;
                            }

                            if (command->iored_input)
                            {
                                char *fileToRead = command->iored_input;
                                in = open(fileToRead, O_RDONLY); // get fd
                                if (in < 0)
                                {
                                    esh_sys_fatal_error("could not open file for reading");
                                }
                                // replace standard input with input file
                                dup2(in, 0);

                                // set redirected input flag for cleanup
                                redirectedIn = true;
                            }

                            // Cleanup input/output file
                            if (redirectedOut)
                                close(out);
                            if (redirectedIn)
                                close(in);

                            // Execute command
                            if (execvp(command->argv[0], command->argv) < 0) {
                                esh_sys_fatal_error("could not execute command");
                            }
                        } else {
                            //this is the parent/./esh
                            command->pid = child;

                            //set pgrp
                            if (pipe->pgrp == -1) {
                                pipe->pgrp = child; //set pgrp as the pid of the first child process (first command run in pipe)
                            }

                            //set pgid of the child to be the pgid of the pipe
                            if (setpgid(child, pipe->pgrp)) {
                                esh_sys_fatal_error("Could not set pgid");
                            }

                            //add job to list of jobs
                            if (!jobAdded) {
                                if (pipe->bg_job) {
                                    printf("[%d] %d\n", pipe->jid, pipe->pgrp);
                                    pipe->status = BACKGROUND;
                                } else {
                                    pipe->status = FOREGROUND;
                                    //FOREGROUND PROCESS CALL WAIT FOR JOB
                                }
                                list_push_back(&jobs, list_pop_front(&cline->pipes));
                                jobAdded = true;
                            }
                        }
                    }
                }
            } // loop through commands

            //wait for the job to complete
            if (jobAdded) {
                wait_for_job(pipe);
            }

            // Give terminal back to ./esh
            give_terminal_to(termId, term);

            // Unblock signal
            esh_signal_unblock(SIGCHLD);

            //put next pipeline in place
            p = placehold;

            //end pipeline while loop
        }

        esh_command_line_free(cline);

    }
    return 0;
}

/**
 * Local function used to print the current jobs when jobs built in is executed
 */
static void print_jobs()
{
    //go through jobs list and print
    struct list_elem *e = list_begin(&jobs);
    struct esh_pipeline *p;

    for (; e != list_end(&jobs); e = list_next(e))
    {
        p = list_entry(e, struct esh_pipeline, elem);

        if (p != NULL)
        {
            printf("[%d] ", p->jid); //print job id

            //print job status
            if (p->status == 0 || p->status == 1)
            {
                printf("Running              ");
            }
            else
            {
                printf("Stopped              ");
            }

            printf("(");
            print_commands(p);
            printf(")\n");
        }
    }
}

static void print_commands(struct esh_pipeline *p) {
    //print command(s) associated with this job
    struct list_elem *e = list_begin(&p->commands);
    struct esh_command *c;
    bool first = true;
    for (; e != list_end(&p->commands); e = list_next(e))
    {
        if (!first) {
            printf(" | ");
        }

        c = list_entry(e, struct esh_command, elem);
        char **chars = c->argv;
        while (*chars)
        {
            printf("%s", *chars);

            //flush -> prevent problems
            fflush(stdout);

            chars++;
            if (*chars != NULL)
            {
                printf(" ");
            }
        }

        first = false;
    }
}

/*
 * Local function used to find a job from the list of jobs by its jid.
 * Used by fg, bg, kill, and stop.
 * @param jid is the job id being searched for
 * @return ptr to esh_pipeline if found, NULL if not.
 */
static struct esh_pipeline *find_jid(int jid)
{
    struct esh_pipeline *p;
    struct list_elem *e;
    e = list_begin(&jobs);
    for (; e != list_end(&jobs); e = list_next(e))
    {
        p = list_entry(e, struct esh_pipeline, elem);

        if (p->jid == jid)
        {
            return p;
        }
    }

    return NULL;
}

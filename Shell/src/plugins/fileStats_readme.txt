The 'fileStats' esh plugin
---------------------------------------------
fileStats is a builtin plugin that given the name of a file will print select
metadata of the file as recorded by stat(2) (http://linux.die.net/man/2/stat).

If the file cannot be found the builtin prints 'No file found by that name.'

If no file is provided, the builtin prints 'No filename provided.'

Usage
---------------------------------------------
fileStats esh.c

Output
---------------------------------------------
No file found by that name.
---or---
No filename provided.
---or---
Size (bytes): 18674
Time of
    Last Access:        Fri Feb 20 05:57:14 2015
    Last Modification:  Fri Feb 20 05:57:14 2015
    Last Status Change: Fri Feb 20 05:57:14 2015

---------------------------------------------
Created by Sam Muggleworth (sammugg@vt.edu) and Nathaniel Hughes (njh2986@vt.edu)
CS 3214 - Spring 2015
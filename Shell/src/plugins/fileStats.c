/*
 * An example plug-in, which implements the 'fileStats' command.
 */
#include <stdbool.h>
#include <stdio.h>
#include <pwd.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include "../esh.h"
#include <signal.h>
#include "../esh-sys-utils.h"

static bool
init_plugin(struct esh_shell *shell)
{
    printf("Plugin 'fileStats' initialized...\n");
    return true;
}

/* Implement file built-in.
 * Returns true if handled, false otherwise. */
static bool
file_builtin(struct esh_command *cmd)
{
    if (strcmp(cmd->argv[0], "fileStats"))
        return false;

    if (cmd->argv[1]) {
        char *filename = cmd->argv[1];

        //check if file exists
        struct stat st;
        if (stat(filename, &st) == 0) {
            printf("Size (bytes): %jd\nTime of\n    Last Access:        %s    Last Modification:  %s    Last Status Change: %s",
                st.st_size, asctime(gmtime(&st.st_atime)), asctime(gmtime(&st.st_mtime)), asctime(gmtime(&st.st_ctime)));
        }
        else {
            printf("No file found by that name.\n");
        }
    }
    else {
        printf("No filename provided.\n");
    }

    return true;
}

struct esh_plugin esh_module = {
    .rank = 1,
    .init = init_plugin,
    .process_builtin = file_builtin
};
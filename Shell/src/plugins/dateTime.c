#include <time.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>
#include "../esh.h"

static bool
init_plugin(struct esh_shell *shell)
{
    printf("Plugin 'dateTime' initialized...\n");
    return true;
}

static bool
dateTime_builtin(struct esh_command *cmd)
{
	
    if (strcmp(cmd->argv[0], "dateTime"))
        return false;
	else 
	{
		time_t dateTime = time(NULL);
		printf("Local time: %s", asctime(localtime(&dateTime)));
		return true;
	}
}

struct esh_plugin esh_module = {
    .rank = 10,
    .init = init_plugin,
    .process_builtin = dateTime_builtin
};

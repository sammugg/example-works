Student Information
-------------------

Nathaniel Hughes
PID: njh2986

Sam Muggleworth
PID: sammugg


How to execute the shell
------------------------

To exectue the esh shell from the command line, run:
./esh

To execute the shell from the command line and initialize a plugin, run:
./esh -p <directory path>
where <directory path> is replaced by the location of the plugin shared library file.

Important Notes
---------------

1) We implemented a simple additonal built in command 'exit' that when run will exit 
./esh back to the calling terminal. It will not exit if there are active jobs.
2) Our esh will not pipe commands separated by the | character, but it will run each 
of them.
3) With the versions of the tests that we had, we received 50/50 on basic tests and
25/50 on advanced tests (I/O and Exclusive Access imp'd).

Description of Base Functionality
---------------------------------

Each builtin command is explicitly recognized by the esh by comparing the argv[0] of
each command to "jobs", "fg", "bg", "kill", and "stop".

jobs:
When the jobs command is run, it calls print_jobs which will iterate through the list 
of active jobs and print out their jid, Running or Stopped status, and their command 
line (ie.(sleep 10), or (ps -ef | grep root)).

fg, bg, kill, and stop:
These commands are recgonized as a unit as they require similar information from the 
user.

If there are no jobs, then fg and bg will say that no current job was found, and kill 
and stop will say that they are missing a jid (as they are required to have one).

With active jobs:
If the user enters any of these commands exclusively, with no addt'l arguments, then 
only fg and bg will actually run. Kill and Stop will say that no job was specified. 
Fg/bg will get the most recently created job and manipulate it as indicated.

If a jid is provided, it must have the format %%%d, as specified in eshoutput.py. So, 
if a second argument is present and it does not have % as the first character, the 
shell will declare that it needs %% in order to run.

If a proper jid is provided, then it will search for the job that has a matching jid 
and return it to be manipulated.

Job control:
fg -> The fc command will block SIGCHLD, report that the job is back in the foreground 
(and change the jobs status to FOREGROUND), and lets the job continue to run by sending 
it the SIGCONT signal using the kill() command and giving it the terminal with 
give_terminal_to(). Then it waits for the job to complete and when it does gets the 
terminal back and unblocks SIGCHLD.

bg -> the bg command simply changes the requested job's status to BACKGROUND and sends 
it the SIGCONT signal. It DOES NOT give it control of the terminal.

stop -> Sends the requested job the SIGSTOP signal.

kill -> Sends the requested job the SIGKILL signal.

Additional job control occurs in the child_status_change function (removing from jobs 
list, ^C and ^Z with foreground jobs, SIGKILL and SIGSTOP response, etc.).

----------------------------------------
Description of Extended Functionality
----------------------------------------

I/O Redirection:
To implement I/O redirection required substituting the file descriptor of the file being 
read from or written to for the descriptor of standard input (0) or standard output(1), 
respectively. After a file is opened, the substitution for stdin or stdout is made using 
the dup(2) system call. This allows for the familiar I/O redirection using '<' and '>.'
To allow for appending output to a file (via the '>>' symbol), the flags in the call to
open the file are changed.

Exclusive Access:
Exclusive access is managed through the give_terminal_to function.  When the shell starts,
the pid of ./esh is recorded and ther terminal is initialized with esh_sys_tty_init() and 
saved in the variable 'struct termios *term.' The terminal is initially given to ./esh.
Then, whenever there is a foreground process, it is given exclusive access to the terminal 
by saying give_terminal_to(pgrp, term), where pgrp is the group id of the foreground job. 
As soon as a job exits the foreground (whether by completion, killing, stopping, etc.), the
terminal is given back to ./esh with give_terminal_to(termId, term).

Piping: Not implemented.

----------------------------------------
List of Plugins Implemented
----------------------------------------

Our Plugins:
    dateTime: This is a built in plugin that prints the local date and time to the 
    	      user's terminal.
    fileStates: This is a built in plugin that given the name of a file will print 
    		select metadata of that file as recorded by stat(2) 
		(http://linux.die.net/man/2/stat).

Plugins from other Groups:
    akatkov_treiter/cowsay
    alisherp_amind1/multTable
    alisiraj_neily1/vt
    alisiraj_neily1/ffc
    alisiraj_neily1/simple_interest
    deanbr_karl88/convertIntToRmNum
    eyobest_ning/bmi_calculator
    jareds94_coreym94/connectfour
    jareds94_coreym94/convertnum
    samja_joevt/temp_converter
    akatkov_treiter/translate
    amsorr_canadian/pwd_prompt
    andrjc4_mfeneley/systemInfo
    banth94_crutcher/customPropmt
    bgill93_ethan01/piglatin
    bgill92_ethan01/sum
    ching93_armendo/tipcal
    chrispc_kbuch5/letsgo
    conorpp_nluther/gcd
    ianjvb_ldp91/heart

* Our shell successfully ran all plugins that were initialized.
* The only plugins that couldn't be initialized were those that didn't allow global access.
    

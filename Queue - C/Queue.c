// On my honor:
//
// - I have not discussed the C language code in my program with
// anyone other than my instructor or the teaching assistants
// assigned to this course.
//
// - I have not used C language code obtained from another student,
// or any other unauthorized source, either modified or unmodified.
//
// - If any C language code or documentation used in my program
// was obtained from another source, such as a text book or course
// notes, that has been clearly noted with a proper citation in
// the comments of my program.
//
// Samuel Muggleworth

#include "Queue.h"

// Return pointer to the rear guard; useful for traversal logic.
//
// Pre:  pQ points to a Queue object
// Returns pointer rear guard element.
//
static const QNode* const Queue_End(const Queue* const pQ);

//
//Helper function for Queue_Reverse.
//Pre: *curr and *pQ are proper
//Post: *pQ contains exactly the same nodes as before, but in reverse order
//
static void ReverseHelp(QNode* curr, Queue* const pQ);

// Initialize QNode pointers to NULL.
//
// Pre:  pN points to a QNode object
// Post: pN->prev and pN->next are NULL
//
void QNode_Init(QNode* const pN) {
	pN->prev = NULL;
	pN->next = NULL;
}

// Initialize Queue to empty state.
//
// Pre:  pQ points to a Queue object
// Post: *pQ has been set to an empty state (see preceding comment
//
void Queue_Init(Queue* const pQ) {
	pQ->fGuard.next = &(pQ->rGuard);
	pQ->fGuard.prev = NULL;
	
	pQ->rGuard.prev = &(pQ->fGuard);
	pQ->rGuard.next = NULL;
}

// Return whether Queue is empty.
//
// Pre:  pQ points to a Queue object
// Returns true if *pQ is empty, false otherwise
//
bool Queue_Empty(const Queue* const pQ){

	if (pQ->fGuard.next == &(pQ->rGuard)) return true;
	
	return false;
}

// Insert *pNode as last interior element of Queue.
//
// Pre:  pQ points to a Queue object
//       pNode points to a QNode object
// Post: *pNode has been inserted at the rear of *pQ
//
void Queue_Push(Queue* const pQ, QNode* const pNode) {
	pNode->next = &(pQ->rGuard); //set pNode next
	pNode->prev = pQ->rGuard.prev; //set pNode prev
	pQ->rGuard.prev->next = pNode; //set prev node next to pnode
	pQ->rGuard.prev = pNode; //set rGuard prev
}

// Remove first interior element of Queue and return it.
//
// Pre:  pQ points to a Queue object
// Post: the interior QNode that was at the front of *pQ has been removed
// Returns pointer to the QNode that was removed, NULL if *pQ was empty
//
QNode* const Queue_Pop(Queue* const pQ) {
	if (Queue_Empty(pQ)) return NULL;
	
	QNode* it = Queue_Front(pQ);

	it->next->prev = &(pQ->fGuard);
	pQ->fGuard.next = it->next;
	
	return it;
}

// Return pointer to the first interior element, if any.
//
// Pre:  pQ points to a Queue object
// Returns pointer first interior QNode in *pQ, NULL if *pQ is empty
//
QNode* const Queue_Front(const Queue* const pQ) {
	if (Queue_Empty(pQ)) return NULL;
	
	QNode* frontElement = pQ->fGuard.next;

	return frontElement;
}

// Return pointer to the last interior element, if any.
//
// Pre:  pQ points to a Queue object
// Returns pointer last interior QNode in *pQ, NULL if *pQ is empty
//
QNode* const Queue_Back(const Queue* const pQ) {
	if (Queue_Empty(pQ)) return NULL;
	
	QNode* lastElement = Queue_End(pQ)->prev;
	
	return lastElement;
}

// Return pointer to the rear guard; useful for traversal logic.
//
// Pre:  pQ points to a Queue object
// Returns pointer rear guard element.
//
static const QNode* const Queue_End(const Queue* const pQ) {
	const QNode* rear = &(pQ->rGuard);

	return rear;
}

// Reverse the order of the interior nodes in the Queue.
//
// Pre:  pQ points to a proper Queue object
// Post: *pQ contains exactly the same nodes as before, but in reverse order
//
void Queue_Reverse(Queue* const pQ) {
	if (Queue_Empty(pQ)) return;
	
	ReverseHelp(Queue_Front(pQ), pQ);
}

//
//Helper function for Queue_Reverse.
//Pre: *curr and *pQ are proper
//Post: *pQ contains exactly the same nodes as before, but in reverse order
//
static void ReverseHelp(QNode* curr, Queue* const pQ) {
	QNode* temp = NULL;
	
	if (curr->prev == &(pQ->fGuard)) {
		//this is the first element in the list
		temp = curr->next;
		ReverseHelp(curr->next, pQ);
		curr->next = &(pQ->rGuard);
		curr->prev = temp;
		pQ->rGuard.prev = curr;
	} else if (curr->next == &(pQ->rGuard)) {
		//this is the last element in the list
		temp = curr->prev;
		curr->prev = &(pQ->fGuard);
		curr->next = temp;
		pQ->fGuard.next = curr;
	} else {
		//this is an internal element
		temp = curr->next;
		ReverseHelp(curr->next, pQ);
		curr->next = curr->prev;
		curr->prev = temp;
	}
}

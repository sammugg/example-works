
// -------------------------------------------------------------------------
/**
 *  ADT for the Node of a Binary tree.
 *  Can be implemented with additional data as user sees fit.
 *  @param <E>
 *
 *  @author Sam
 *  @version Feb 10, 2014
 */
public interface Node<E>
{
    /**
     * returns the left child of the node.
     * @return the left child.
     */
    public Node<E> left();

    /**
     * returns the right child of the node.
     * @return the right child.
     */
    public Node<E> right();

    /**
     * sets the element of the node
     * @param e the element stored at the node
     */
    public void setElement(E e);

    /**
     * returns the element of the node.
     * @return the element.
     */
    public E element();
}

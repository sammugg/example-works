
import java.util.List;

// -------------------------------------------------------------------------
/**
 *  This interface represents the core functions of a BST.  When implemented,
 *  it will create a generic bst class that can be used to insert, remove,
 *  search, and find elements of a database.
 *
 *  @author Sam
 *  @version Feb 11, 2014
 *  @param <E> element
 *  @param <K> Key
 */
public interface BSTADT<K, E>
{
    /** Clears and resets the BST */
    public void clear();

    // ----------------------------------------------------------
    /** Returns whether or not the BST is empty.
     * @return true || false
     */
    public boolean isEmpty();

    // ----------------------------------------------------------
    /**
     * Inserts a new node
     * @param k key for new node
     * @param e Element of new node
     */
    public void insert(K k , E e);

    // ----------------------------------------------------------
    /**
     * remove the node that has the key k and return its element.
     * replace node with smallest element (left most) of right subtree.
     * @param e the element to find and remove
     */
    public void removeElement(E e);

    // ----------------------------------------------------------
    /**
     * Find the node with the key k and return its element
     * @param k key of element to find
     * @return the element if found, otherwise null.
     */
    public List<E> find(K k);

    // ----------------------------------------------------------
    /**
     * Find and print the value at the k index as if all values were listed
     * in ascending order.
     * @param k index of element to find (kth element)
     * @return the element, null if out of range
     */
    public E findIndex(int k);

    // ----------------------------------------------------------
    /**
     * Find and print the elements in the requested range
     * @param min lower bound of search
     * @param max upper bound of search
     * @return list of elements found
     */
    public List<E> findRange(K min, K max);

    /** returns inorder traversal of the tree */
    //public void visitInorder();
}

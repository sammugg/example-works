
// -------------------------------------------------------------------------
/**
 *  Implements the Node ADT.
 *  A BST node will have a left and right child, an element, and will also
 *  store the number of elements to its right and left.
 *  @param <E> type to be stored in the BST
 *  @param <K> the key
 *  @author Sam
 *  @version Feb 10, 2014
 */
public class BSTNode<K, E> implements Node<E>
{
    private int index;
    private BSTNode<K, E> left;
    private BSTNode<K, E> right;
    private E element;
    private K key;


    // ----------------------------------------------------------
    /**
     * Constructor for the BSTNode class.
     * @param k key value
     * @param e element of node
     */
    public BSTNode(K k, E e) {
        setElement(e);
        setKey(k);
        left = null;
        right = null;
        index = 0;
    }

    // Count modifications ----------------------------------------------------
    /** increases count for # of nodes to left. */
    public void incIndex() { index++; }

    /** decreases count for # of nodes to left. */
    public void decIndex() { index--; }

    /** used to set the index if a simple ++ or -- won't suffice
     * @param i the new index # */
    public void setIndex(int i) { this.index = i; }

    /**
     * returns the nodes index number.
     * @return leftCount
     */
    public int index() { return index; }

    // Child modifications-----------------------------------------------------
    @Override
    public BSTNode<K, E> left() { return left; }

    /**
     * Sets the left child of this node..
     * @param l value for the left child
     */
    public void setLeft(BSTNode<K, E> l) { this.left = l; }

    @Override
    public BSTNode<K, E> right() { return right; }

    /**
     * sets right child of this node.
     * @param r value for the right child
     */
    public void setRight(BSTNode<K, E> r) { this.right = r; }

    // Element modification ---------------------------------------------------
    @Override
    public void setElement(E e) { this.element = e; }

    @Override
    public E element() { return element; }

    // ----------------------------------------------------------
    /** @return the key */
    public K key() { return key; }

    // ----------------------------------------------------------
    /** @param key the key to set */
    public void setKey(K key) { this.key = key; }
}

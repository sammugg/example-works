// import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Comparator;

// -------------------------------------------------------------------------
/**
 * Implementation of the BSTADT to create a Generic Binary Search Tree class.
 * GenericBST can initialized to perform operations on a database given that it
 * has a Comparable key type and proper Comparator for elements.
 *
 * @param <K>
 *            Key type
 * @param <E>
 *            Element type
 * @author Sam
 * @version Feb 13, 2014
 */
public class GenericBST<K extends Comparable<K>, E>
    implements BSTADT<K, E>
{
    /**
     * this node is protected for testing purposes. it can't be modified, but it
     * can be accessed.
     */
    protected BSTNode<K, E> root;     // the root node of the BST.
    private int             count;    // keeps track of how many nodes are in
// the
// tree.
    private Comparator<E>   comp;     // comparator for elements in the tree.
// used in removeElement to determine if the index of a node should be decreased
    private boolean         isRemoved;


    /**
     * Constructs a new instance of a BST
     *
     * @param comparator
     *            is the Comparator<E> used to make traversal decisions.
     */
    public GenericBST(Comparator<E> comparator)
    {
        this.comp = comparator;
        this.root = null;
        this.count = 0;
        this.isRemoved = false;
    }


    // Housekeeping -----------TESTED------------------------------------------
    /** Clears and resets the BST */
    public void clear()
    {
        this.root = null;
        this.count = 0;
    }


    /**
     * returns current count
     *
     * @return the count
     */
    public int count()
    {
        return count;
    }


    /**
     * Returns whether or not the BST is empty.
     *
     * @return true || false
     */
    public boolean isEmpty()
    {
        return (this.count == 0);
    }


    // INSERT FUNCTIONS-------TESTED------------------------------------------
    /**
     * Inserts a new node
     *
     * @param k
     *            key for new node
     * @param e
     *            Element of new node
     */
    public void insert(K k, E e)
    {
        // create a new node instance so that the index # can be adjusted
        BSTNode<K, E> node = new BSTNode<K, E>(k, e);

        if (isEmpty())
        {
            root = node;
        }
        else
        {
            root = inserthelp(root, node);
        }
        count++;
    }


    /** does the grunt work for the insert function */
    private BSTNode<K, E> inserthelp(BSTNode<K, E> rt, BSTNode<K, E> node)
    {
        if (rt == null)
        {
            return node;
        }

        else if (comp.compare(node.element(), rt.element()) >= 0)
        {
            // if new element is >= curr node element
            // change the new nodes index so that it is 1 + rt.index()
            rt.setRight(inserthelp(rt.right(), node));
        }
        else
        {
            // if new element is < curr node element
            // increase rt index by 1 because there is one more node to left
            rt.incIndex();
            rt.setLeft(inserthelp(rt.left(), node));
        }
        // return the current root in order to maintain tree order.
        return rt;
    }


    // REMOVE FUNCTIONS---------TESTED----------------------------------------


    /**
     * finds and returns the minimum value from the current tree. This will
     * always be the farthest left node of that tree.
     *
     * @param rt
     *            the node being evaluated
     * @return the left most node
     */
    public BSTNode<K, E> getmin(BSTNode<K, E> rt)
    {
        if (rt == null)
        {
            return null;
        }
        if (rt.left() == null)
        {
            // this is the left most node
            return rt;
        }
        else
        {
            return getmin(rt.left());
        }
    }


    /**
     * removes the smallest node in the tree (left most node)
     *
     * @param rt
     *            the root of current tree.
     * @return the node replacing smallest, null if nothing
     */
    public BSTNode<K, E> deletemin(BSTNode<K, E> rt)
    {
        if (rt == null)
        {
            return null;
        }
        else if (rt.left() == null)
        {
            return rt.right();
        }
        else
        {
            rt.setLeft(deletemin(rt.left()));
            rt.decIndex();
        }

        return rt;
    }


    /**
     * Deletes a node from the tree containing the element e.
     *
     * @param e
     *            the element being removed
     */
    public void removeElement(E e)
    {
        // Now remove the node if found
        this.isRemoved = false;
        removehelp(root, e);
        if (this.isRemoved)
        {
            count--;
        }
    }


    /**
     * Helps remove an element from the tree. Recursive. Note: this
     * implementation removes only one instance of a record from the bst. If for
     * some reason a record is referenced more than once, removehelp must be
     * called again.
     *
     * @param rt
     *            the current node, root of tree/subtree
     * @param e
     *            the element requested to be removed.
     * @return a node, either the current or a replacement
     */
    protected BSTNode<K, E> removehelp(BSTNode<K, E> rt, E e)
    {
        if (rt == null)
        {
            return null;
        }
        else if (comp.compare(rt.element(), e) > 0)
        {
            rt.setLeft(removehelp(rt.left(), e));
            if (this.isRemoved)
            {
                rt.decIndex();
            }
        }
        else if (comp.compare(rt.element(), e) < 0)
        {
            // the current roots element is less than e, go right
            rt.setRight(removehelp(rt.right(), e));
        }
        else
        { // found a possible match
            if (e == rt.element())
            {
                // correct element found. remove it.
                this.isRemoved = true;
                if (rt.left() == null)
                {
                    return rt.right();
                }
                else if (rt.right() == null)
                {
                    return rt.left();
                }
                else
                {
                    // Two children. replace current node with minimum (farthest
                    // left) of right subtree.
                    BSTNode<K, E> temp = getmin(rt.right());
                    rt.setElement(temp.element());
                    rt.setKey(temp.key());
                    rt.setRight(deletemin(rt.right()));
                }
            }
            else
            {
                rt.setRight(removehelp(rt.right(), e));
            }
        }

        return rt;
    }


    // Find ------------------TESTED------------------------------------------
    /**
     * Find all nodes with the key k and return its element
     *
     * @param k
     *            key value to find
     * @return the elements, if found, otherwise null.
     */
    public List<E> find(K k)
    {
        List<E> list = new LinkedList<E>();
        findHelp(root, k, list);
        if (list.isEmpty())
        {
            return null;
        }
        return list;
    }


    /**
     * Does grunt work for find function
     *
     * @param rt
     *            is the curr node, root of tree/subtree
     * @param k
     *            is the key to find
     * @param list
     *            the list too be modified
     */
    private void findHelp(BSTNode<K, E> rt, K k, List<E> list)
    {
        if (rt == null)
        {
            return;
        } // node not found
        else if (rt.key().compareTo(k) == 0)
        {
            list.add(rt.element());
            findHelp(rt.right(), k, list);
        } // a node was found, keep going to right subtree to check for more
        else if (rt.key().compareTo(k) > 0)
        {
            findHelp(rt.left(), k, list);
        }
        else
        {
            findHelp(rt.right(), k, list);
        }
    }


    // FIND KTH --------------TESTED------------------------------------------
    /**
     * Find and print the value at the k index as if all values were listed in
     * ascending order.
     *
     * @param k
     *            index to find
     * @return the element, null if out of range
     */
    public E findIndex(int k)
    {
        if (k < 0)
        {
            return null;
        } // doesn't exist
        return findKthHelp(root, k);
    }


    /**
     * helper for findIndex function
     *
     * @param rt
     *            curr node, root of tree/subtree
     * @param k
     *            index to find
     * @return the element if found
     */
    private E findKthHelp(BSTNode<K, E> rt, int k)
    {
        if (rt == null)
        {
            return null;
        } // there is no Kth
          // the rt null case occurs if 1) the bst is empty, 2) k is out of
// range

        // if rt index is the same as k, then this is the element that we want
        else if (rt.index() == k)
        {
            return rt.element();
        }
        // what if we rt index is greater? that means k is to the left.
        else if (rt.index() > k)
        {
            return findKthHelp(rt.left(), k);
        }
        // and in the last case, rt index is smaller, so k is to the right.
        return findKthHelp(rt.right(), k - (rt.index() + 1));
    }


    // FIND RANGE ------------TESTED------------------------------------------
    /**
     * Find and print the elements in the requested range
     *
     * @param min
     *            lower bound of search, inclusive
     * @param max
     *            upper bound of search, inclusive
     * @return list of elements found
     */
    public List<E> findRange(K min, K max)
    {
        List<E> found = new LinkedList<E>();

        rangeHelp(min, max, this.root, found);

        if (found.isEmpty())
        {
            return null;
        }
        return found;
    }


    /**
     * does grunt work of find range operation. Modifies a list of results.
     *
     * @param min
     *            the lower bound
     * @param max
     *            the upper bound
     * @param rt
     *            the curr node, root of tree/subtree
     * @param list
     *            the list of results to be modified
     */
    private void rangeHelp(K min, K max, BSTNode<K, E> rt, List<E> list)
    {
        if (rt == null)
        {
            return;
        }
        else if (rt.key().compareTo(max) > 0)
        {
            // query range is to the left
            rangeHelp(min, max, rt.left(), list);
        }
        else if (rt.key().compareTo(min) < 0)
        {
            // query range is to the right
            rangeHelp(min, max, rt.right(), list);
        }
        else
        {
            // in query range, begin adding elements to list in order
            // so, call left range, add curr, call right range
            rangeHelp(min, max, rt.left(), list);
            list.add(rt.element());
            rangeHelp(min, max, rt.right(), list);
        }
    }


    /**
     * returns inorder traversal of the tree
     *
     * @return a linked list of the elements
     */
    public List<E> visitInorder()
    {
        List<E> list = new LinkedList<E>();

        inorder(root, list);

        return list;
    }


    /**
     * sends elements in order to the given list recursive
     *
     * @param rt
     *            the current node, root of tree/subtree
     * @param list
     *            the linked list to be modified
     */
    private void inorder(BSTNode<K, E> rt, List<E> list)
    {
        if (rt == null)
        {
            return;
        }
        inorder(rt.left(), list);
        list.add(rt.element());
        inorder(rt.right(), list);
    }
}
